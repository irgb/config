# -*- encoding: utf-8 -*-

import os
import re
import sys
import yaml

def load_mask(mask_file):
    if not os.path.isfile(mask_file):
        print('mask_file: {} does not exists'.format(mask_file))
        sys.exit(1)
    with open(mask_file, 'rt') as f:
        config = yaml.load(f)
        return config

def render():
    if len(sys.argv) < 2:
        print('mask file is not specified.')
        sys.exit(1)
    mask_file = sys.argv[1]
    mask = load_mask(mask_file)
    for tmpl_file in mask.keys():
        # 检查文件是否存在
        if not os.path.isfile(tmpl_file):
            print('{} does not exist'.format(tmpl_file))
            sys.exit(1)
        # 模版必须以"-template" 为后缀
        tmpl_filename, ext = os.path.splitext(tmpl_file)
        if not tmpl_filename.endswith('-template'):
            print('{} is not a template file, which must with "-template" suffix'.format(tmpl_file))
            sys.exit(1)
        with open(tmpl_file, 'rt') as f:
            tmpl = f.read()
        # template filling
        for key, value in mask[tmpl_file].items():
            if key != key.upper():
                print('key: {} must be upper case'.format(key))
                sys.exit(1)
            tmpl = re.sub(r'\[\[ ?{} ?\]\]'.format(key), value, tmpl)
        # write valid conf file
        conf_file = re.sub('-template', '', tmpl_filename) + ext
        with open(conf_file, 'wt') as f:
            f.write(tmpl)

if __name__ == '__main__':
    sys.exit(render())
