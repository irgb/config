# -*- encoding: utf-8 -*-

from setuptools import setup

setup(
    name='config-tool',
    version='0.0.1',
    author='irgb',
    author_email='expo.tao@gmail.com',
    url='https://bitbucket.org/irgb/config',
    description=u'config-tool',
    packages=['config-tool'],
    install_requires=['yaml'],
    entry_points={
        'console_scripts': [
            'conf_render=config.conf_render:render'
        ]
    }
)
